import { createStore, applyMiddleware } from 'selex';
import { createEpicMiddleware } from 'redux-observable';
import { map, mergeMap, delay } from 'rxjs/operators';

const rootEpic = $action => $action.pipe(
    delay(1000), // Simulate work
    mergeMap(action => Promise.resolve(action)), // Simulate resolved work
    map(action => ({ ...action, type: `${action.type}_COMPLETE` }))
);

export const rootReducer = {
    loading: (state = false, action) => {
        switch(action.type) {
            case 'SET_LOADING':
                return true;
            case 'SET_LOADING_COMPLETE':
                return false;
            default:
                return state;
        }
    }
};

const enhancer = applyMiddleware(createEpicMiddleware(rootEpic));

// Use an enhanced `createStore` function instead of `createStore`
export const createEnhancedStore = enhancer(createStore);

// Create an enhanced store by passing the enhancer into `createStore`
// You can also just pass in an array of middleware.
export const enhancedStore = createStore(rootReducer, { loading: false }, enhancer);
