module.exports = {
    onHandlePlugins(event) {
        const plugins = event.data.plugins;
        
        const customPlugin = p => p.name.match(/^\.\/esdoc\//);
        
        const not = f => x => !f(x);

        const customPlugins = plugins.filter(customPlugin);
        const esdocPlugins = plugins.filter(not(customPlugin));

        event.data.plugins = [...esdocPlugins, ...customPlugins];
    }
};