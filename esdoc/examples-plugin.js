const path = require("path");
const fs = require("fs");

const inject = (content, sourcePath, format) =>
  content.replace(/\[EXAMPLE (.*)\]/g, (_, injectPath) => {
    const codePath = path.resolve(path.dirname(sourcePath), injectPath);
    
    try {
      const code = fs.readFileSync(codePath).toString();
      return format(code);
    } catch (e) {
      console.error(`[ERROR] CANNOT INJECT EXAMPLE ${codePath}`);
      return `[EXAMPLE  ERROR! ${injectPath}]`;
    }
  });

module.exports = {  
  onHandleCode(event) {
    const commentsFormatter = code => {
      const lines = code.split("\n");
      return [lines[0], ...lines.slice(1).map(line => ` * ${line}`)].join("\n");
    };
    
    event.data.code = inject(event.data.code, event.data.filePath, commentsFormatter);
  },
  
  onHandleDocs(event) {
    event.data.docs.forEach(doc => {
      const fencesFormatter = code => `\`\`\`\n${code}\n\`\`\``;
            
      if (doc.longname.match(/\.md$/)) {
        doc.content = inject(doc.content, doc.longname, fencesFormatter);
      }
    });
  }
};
